package pal;

class StringIteration {
    int factorStart;
    int iteration;
    String dictWord;

    StringIteration(String dictWord) {
        this.iteration = 1;
        this.dictWord = dictWord;
    }

    StringIteration(String dictWord, int factorStart) {
        this.factorStart = factorStart;
        this.iteration = 1;
        this.dictWord = dictWord;
    }

    boolean isFinished() {
        return iteration == dictWord.length();
    }

    boolean isNextIterationChar(char next) {
        boolean isNext = next == dictWord.charAt(iteration);
        iteration++;
        return isNext;
    }

    @Override
    public String toString() {
        return "StringIteration{" +
                "factorStart=" + factorStart +
                ", iteration=" + iteration +
                ", dictWord='" + dictWord + '\'' +
                '}';
    }

    public static StringIteration clone(StringIteration image, int factorStart) {
        return new StringIteration(image.dictWord, factorStart);
    }
}
