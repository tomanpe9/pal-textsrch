package pal;

import java.util.ArrayList;
import java.util.HashSet;

class BruteSolution {

    private static MyHashMultimap<Character, StringIteration> stringMap = new MyHashMultimap<>();

    static int factorize(char[] longString, String[] dictionary) {
        prepareDictionary(dictionary);

        int[] factorSize = new int[longString.length + 1];
        int maxFactor = 0;

        ArrayList<StringIteration> nextIteration = new ArrayList<>();
        for (int i = 0; i < longString.length; i++) {
            ArrayList<StringIteration> potentialIteration = new ArrayList<>();
            char c = longString[i];
            int smallestFinished = Integer.MAX_VALUE;
            for (StringIteration iter : nextIteration) {
                if (iter.isNextIterationChar(c)) {
                    if (iter.isFinished()) {
                        int thisFactor = factorSize[iter.factorStart] + 1;
                        maxFactor = thisFactor > maxFactor ? thisFactor : maxFactor;
                        if (iter.iteration < smallestFinished) {
                            smallestFinished = iter.iteration;
                        }
                    } else {
                        potentialIteration.add(iter);
                    }
                }
            }
            ArrayList<StringIteration> newIteration = new ArrayList<>();
            if (smallestFinished < Integer.MAX_VALUE) {
                for (StringIteration iter : potentialIteration) {
                    if (iter.iteration > smallestFinished) {
                        stringMap.removeValue(iter.dictWord.charAt(0), iter);
                    } else {
                        newIteration.add(iter);
                    }
                }
            } else {
                newIteration = potentialIteration;
            }
            if (stringMap.containsKey(c)) {
                for (StringIteration iter : stringMap.get(c)) {
                    newIteration.add(StringIteration.clone(iter, i));
                }
            }
            factorSize[i + 1] = maxFactor;
            nextIteration = newIteration;
        }
        return factorSize[longString.length];
    }

    private static void prepareDictionary(String[] dictionary) {
        HashSet<StringIteration> strings = new HashSet<>();
        char previous = dictionary[0].charAt(0);
        String prevWord = null;
        for (String dictWord : dictionary) {
//            if (dictWord.length() == 1) {
//                  // TODO: Destroy all others!!
//            } else
            if (previous == dictWord.charAt(0)) {
                if (dictWord.equals(prevWord)) {
                    continue;
                }
                strings.add(new StringIteration(dictWord));
            } else {
                stringMap.put(previous, strings);
                strings = new HashSet<>();
                previous = dictWord.charAt(0);
                strings.add(new StringIteration(dictWord));
            }
            prevWord = dictWord;
        }
        stringMap.put(previous, strings);
    }
}
