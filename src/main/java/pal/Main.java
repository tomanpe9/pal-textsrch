package pal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.StringTokenizer;

public class Main {

    private static final String DELIMITER = " ";

    private static String longString;
    private static String[] dictionary;

    /**
     * Running algorithm inside.
     * <p>
     * Reading from System.in and returning solution in System.out
     *
     * @throws IOException on fail to read
     */
    public static void main(String[] args) throws IOException {
        int solution = processSolution(new BufferedReader(new InputStreamReader(System.in)));
        System.out.println(solution);
    }

    /**
     * Moved to new method to allow adding different BufferedReader
     *
     * @param bufferedReader with graph data
     * @return best solution
     * @throws IOException on fail to read
     */
    static int processSolution(BufferedReader bufferedReader) throws IOException {
        processBufferedReaderExample(bufferedReader);
        return BruteSolution.factorize(longString.toCharArray(), dictionary);
    }

    /**
     * Simple example how to read from buffered reader
     *
     * @param bufferedReader buffered reader
     * @throws IOException on fail with buffered reader
     */
    private static void processBufferedReaderExample(BufferedReader bufferedReader) throws IOException {
        String[] text = splitString(bufferedReader.readLine(), DELIMITER);
        longString = text[0];
        text = splitString(bufferedReader.readLine(), DELIMITER);
        int dictNo = Integer.parseInt(text[0]);
        dictionary = new String[dictNo];
        for (int i = 0; i < dictNo; i++) {
            text = splitString(bufferedReader.readLine(), DELIMITER);
            String dictWord = text[0];
            dictionary[i] = dictWord;
        }
        Arrays.sort(dictionary);
    }


    /**
     * BRUTALLY FASTER THEN String.split()
     * <p>
     * Taken from sun - StringUtils
     */
    private static String[] splitString(String line, String delimiter) {
        StringTokenizer tokenizer = new StringTokenizer(line, delimiter);
        String[] splitString = new String[tokenizer.countTokens()];

        for (int var4 = 0; var4 < splitString.length; ++var4) {
            splitString[var4] = tokenizer.nextToken();
        }

        return splitString;
    }

}

