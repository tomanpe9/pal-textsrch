package pal;

import java.util.HashMap;
import java.util.HashSet;

public class MyHashMultimap<K, V> extends HashMap<K, HashSet<V>> {

    public void putValue(K key, V value) {
        HashSet<V> values = get(key);
        if (values != null) {
            values.add(value);
        } else {
            values = new HashSet<>();
            values.add(value);
            put(key, values);
        }
    }

    public void removeValue(K key, V value) {
        HashSet<V> values = get(key);
        if (values != null) {
            values.remove(value);
        }
    }

}

